using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang._sentence.clause._scule
{
	internal enum Li_
	{

		/// <summary>
		/// eg:
		///		Abc def gh
		/// </summary>
		Sentence
			,
		/// <summary>
		/// eg:
		///		Abc Def Gh
		/// </summary>
		/// <see cref="nilnul.txt_.word."/>
		/// <see cref="nilnul.txt_.vered_._id._nom.term.phrase_._casing.Li_.CapitalCase"/>
		Title

	}
}
