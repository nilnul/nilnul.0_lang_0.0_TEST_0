﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.grammar_.contextual_.contextless_.eg_
{
	/// <summary>
	/// eg:
	///		S::aSb
	///		S::ab
	/// </summary>
	/// <remarks>
	/// this defines a set:
	///		{a^n b^n | n>=1}
	/// </remarks>
	internal class NonRegular
	{
	}
}
