﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul.lang.grammar_.contextual_.contextless_.regular_.eg_
{
	/// <summary>
	/// eg:
	///		S::aA
	///		A::aA
	///		A::bB
	///		B::bB
	///		B:: ""
	/// </summary>
	/// <remarks>
	/// this defines: {a^n b^m , where m,n>=1};
	/// </remarks>
	internal class AsBs
	{
	}
}
